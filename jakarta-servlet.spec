%bcond_with bootstrap

Name:           jakarta-servlet
Version:        5.0.0
Release:        4
Summary:        Server-side API for handling HTTP requests and responses
License:        (EPL-2.0 or GPLv2 with exceptions) and ASL 2.0
URL:            https://github.com/eclipse-ee4j/servlet-api
BuildArch:      noarch

Source0:        https://github.com/eclipse-ee4j/servlet-api/archive/%{version}-RELEASE/servlet-api-%{version}.tar.gz

BuildRequires:  maven-local
%if %{with bootstrap}
BuildRequires:  javapackages-bootstrap
%else
BuildRequires:  mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)
%endif
provides:       glassfish-servlet-api = %{version}-%{release}
Conflicts:      glassfish-servlet-api < %{version}-%{release}

%description
Jakarta Servlet defines a server-side API for handling HTTP requests
and responses.

%{?javadoc_package}

%prep
%setup -q -n servlet-%{version}-RELEASE

%pom_remove_parent . api

%pom_disable_module spec

cp -pr api/src/main/java/jakarta api/src/main/java/javax
sed -i -e 's/jakarta\./javax./g' $(find api/src/main/java/javax -name *.java)
%pom_xpath_replace pom:instructions/pom:Export-Package \
  '<Export-Package>jakarta.servlet.*,javax.servlet.*;version="4.0.0"</Export-Package>' api

%mvn_package jakarta.servlet:servlet-parent __noinstall

%pom_remove_plugin -r :formatter-maven-plugin
%pom_remove_plugin -r :impsort-maven-plugin
%pom_remove_plugin -r :maven-enforcer-plugin
%pom_remove_plugin -r :maven-javadoc-plugin
%pom_remove_plugin -r :maven-source-plugin

%mvn_alias jakarta.servlet:jakarta.servlet-api \
    javax.servlet:javax.servlet-api \
    javax.servlet:servlet-api

%mvn_file :{*} %{name}/@1 glassfish-servlet-api

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE.md NOTICE.md
%doc README.md

%changelog
* Tue Feb 28 2023 xu_ping <xuping33@h-partners.com> - 5.0.0-4
- Add Confilict glassfish-servlet-api due to provides same file glassfish-servlet-api.jar

* Wed Nov 16 2022 wangkai <wangkai385@h-partners.com> - 5.0.0-3
- Change source

* Sat Feb 12 2022 Ge Wang <wangge20@huawei.com> - 5.0.0-2
- Fix install confilict with glassfish-servlet-api due to provides same file glassfish-servlet-api.jar

* Thu Dec 23 2021 houyingchao<houyingchao@huawei.com> - 5.0.0-1
- Initial package
